# Apache2 Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [Apache](https://httpd.apache.org/) server resources.
