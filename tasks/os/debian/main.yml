# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/apt_module.html
- name:
    "package"
  apt:
    cache_valid_time: "{{ apt_cache_valid_time | default(omit) }}"
    default_release: "{{ apt_default_release | default(omit) }}"
    name: "{{ apache2_package_name }}"
    state: "{{ apache2_package_state }}"
    update_cache: "{{ apt_update_cache | default(omit) }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/apt_module.html
- name:
    "module packages"
  apt:
    cache_valid_time: "{{ apt_cache_valid_time | default(omit) }}"
    default_release: "{{ apt_default_release | default(omit) }}"
    name: "{{ apache2_module_package_name }}"
    state: "{{ apache2_module_state }}"
    update_cache: "{{ apt_update_cache | default(omit) }}"
  vars:
    apache2_module_included:
      "{{ item.value['package_name'] is not defined
          and not item.value['package'] | default(false)
        }}"
    apache2_module_package_name:
      "{{ item.value['package_name']
        | default('libapache2-mod-' + item.key)
        }}"
    apache2_module_properties:
      "{{ apache2_module_included
        | ternary({'included': true}, {'state': apache2_module_state})
        }}"
    apache2_module_state:
      "{{ item.value['state'] | default('present') }}"
  when:
    - "not apache2_module_included"
  loop:
    "{{ apache2_modules | dict2items }}"
  loop_control:
    label: "{{ [item.key, apache2_module_properties] | to_json }}"
  become:
    true
